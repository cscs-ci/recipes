# Gitlab CI recipes for CSCS

This repo provides recipes to build and run containers on Piz Daint for continuous integration.

In case you want to use it, make sure the `daint-container` and `kubernetes` runners are [enabled for your project](https://docs.gitlab.com/ce/ci/runners/), which is the default for any project under the `cscs-ci` organization. Also ensure you include the latest version of the recipes in your `.gitlab-ci.yml`:

```yaml
include:
  - remote: 'https://gitlab.com/cscs-ci/recipes/-/raw/master/templates/v2/.cscs.yml'

...
```

## Basic usage

### Building an image on Kubernetes

When you extend `.dind` (docker-in-docker), you have `docker` available as a command in your script. You can use Gitlab's environment variables to give the image a proper name, and push it to your repository's registry to store it for later use.

```yaml
...

stages:
  - build
  - test

build:
  extends: .dind
  stage: build
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/release:$CI_COMMIT_SHA
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $IMAGE --network=host .
    - docker push $IMAGE

...
```

### Running the container on Piz Daint

By extending `.daint` in your job and providing an image, you can run a script inside a container on Piz Daint:

```yaml
...

test:
  extends: .daint
  stage: test
  image: $CI_REGISTRY_IMAGE/release:$CI_COMMIT_SHA
  script: echo "My CI script"
  variables:
    PULL_IMAGE: 'YES'
```

This will allocate one node on the `cscsci` partition on `daint-mc` with a single task executing the provided script inside the provided container.

To change the slurm configuration, set [srun input environment variables](https://slurm.schedmd.com/srun.html#lbAI). For instance:

```yaml
...

test:
  extends: .daint
  stage: test
  image: $CI_REGISTRY_IMAGE/release:$CI_COMMIT_SHA
  variables:
    SLURM_CONSTRAINT: daint-gpu
    SLURM_NTASKS: 8
    PULL_IMAGE: 'YES'
  script: echo "My CI script"
```

## Multiple srun commands under a single allocation

The rule of thumb is to have **one ci job per `srun` command**. For security reasons it is impossible to use `sbatch` to execute multiple `srun` commands within a single allocation. As an alternative however, one can define an allocation job followed by as many jobs as `srun` commands. For instance:

```yaml
include:
  - remote: 'https://gitlab.com/cscs-ci/recipes/-/raw/master/templates/v2/.cscs.yml'

stages:
  - build
  - allocate
  - run
  - cleanup

...

# Use this image for subsequent jobs
image: $CI_REGISTRY_IMAGE/release:$CI_COMMIT_SHA

# Set ALLOCATION_NAME to something unique
variables:
  ALLOCATION_NAME: build-things-$CI_PIPELINE_ID
  SLURM_CONSTRAINT: daint-gpu
  SLURM_JOB_NODES: 2

# Allocate the resources
allocate:
  variables:
    PULL_IMAGE: 'YES'
  stage: allocate
  extends: .daint_alloc

# Execute multiple jobs
test1:
  stage: run
  variables:
    SLURM_NTASKS: 4
  extends: .daint
  script: echo "Hello world from job 1"

test2:
  stage: run
  variables:
    SLURM_NTASKS: 8
  extends: .daint
  script: echo "Hello world from job 2"

# Remove the allocation
deallocate:
  stage: cleanup
  extends: .daint_dealloc
```

## Advanced

More advanced usage includes speeding up docker builds by caching layers and running a dynamic number of `srun` commands on daint.

### Caching the build environment when building docker images

For larger projects it is useful to split the build into two: one `build` image for dependencies that rarely chance, and one `deploy` image for the current project. To accomplish that, try something along the following lines, where we fix the tag for the build environment to `latest` for reuse in later pipelines, and use `--cache-from` to enable caching, and pass `--build-arg BUILD_ENV=$BUILD_IMAGE` to the `deploy` dockerfile such that it can extend it. The current Kubernetes setup uses some experimental docker features (Buildkit to be precise), which currently requires you to set the flag `--build-arg BUILDKIT_INLINE_CACHE=1`. Note that the `deploy` image can now be a small, minimal image by exploiting multi-stage builds to drop all traces of development tools.

```yaml
include:
  - remote: 'https://gitlab.com/cscs-ci/recipes/-/raw/master/templates/v2/.cscs.yml'

build:
  extends: .dind
  variables:
    BUILD_IMAGE: $CI_REGISTRY_IMAGE/build:latest
    DEPLOY_IMAGE: $CI_REGISTRY_IMAGE/deploy:$CI_COMMIT_SHA
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $BUILD_IMAGE --cache-from $BUILD_IMAGE --build-arg BUILDKIT_INLINE_CACHE=1 -f docker/build-env-debug.Dockerfile --network=host .
    - docker push $BUILD_IMAGE
    - docker build -t $DEPLOY_IMAGE --build-arg BUILD_ENV=$BUILD_IMAGE -f docker/deploy-debug.Dockerfile --network=host .
    - docker push $DEPLOY_IMAGE
```

### Working with a dynamic number of srun commands

In case a static files like the above do not fit your use case because the number of srun commands varies between pull requests or is generally unknown in advance, it is possible to [generate the pipeline file](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html#dynamic-child-pipelines) like above, and execute it as a child pipeline. For example:

```yaml

stages:
  - build
  - test

build:
  stage: build
  script:
    - build a thing
    - generate_yaml > pipeline.yml # this generates a file like the pipelines above
  artifacts:
    paths:
      - pipeline.yml

test:
  stage: test
  trigger:
    include: # use the pipeline file from the build job
      - artifact: pipeline.yml
        job: build
```


## Environment variables that control how jobs are run

You can use most of the default `srun` related environment variables to control how slurm is called. In particular, see:

**https://slurm.schedmd.com/srun.html** section **INPUT ENVIRONMENT VARIABLES**.

There are some variables to configure the container runtime as well:


| Variable               | Values (default in bold)  | Description                                                                                                                                                            |
|------------------------|---------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `PULL_IMAGE`           | `YES` / **`NO`**          | When set to `YES`, the image will be pulled from the registry                                                                                                          |
| `REGISTRY_USER`        | (string)                  | Username for pulling from private registries. For private Gitlab repositories, use `$CI_REGISTRY_USER`                                                                 |
| `REGISTRY_PASSWORD`    | (string)                  | Password for pulling from private registries. For private Gitlab repositories, use `$CI_REGISTRY_PASSWORD`                                                             |
| `ALLOCATION_NAME`      | (string)                  | Name for the Slurm allocation. Useful when running multiple jobs without having to allocate resources each time. Typical value is `my-pipeline-$CI_PIPELINE_ID`.       |
| `ONLY_ALLOCATE`        | `1` / **`0`**             | Set to `1` to create a slurm allocation with the name `$ALLOCATION_NAME`. The `before_script`, `script` and `after_script` will not be executed.                       |
| `ONLY_DEALLOCATE`      | `1` / **`0`**             | Set to `1` to cancel a slurm allocation with the name `$ALLOCATION_NAME`. The `before_script`, `script` and `after_script` will not be executed.                       |
| `DISABLE_AFTER_SCRIPT` | `YES` / **`NO`**          | Set to `YES` to disable the `after_script`. This removes the overhead of running an empty `after_script` command through slurm.                                        |
| `USE_MPI`              | `YES` / **`NO`**          | Set to `YES` to enable MPI communication betwee containers                                                                                                             |
| `CRAY_CUDA_MPS`        | (unset)                   | This variable must be set to make one GPU available to multiple containers.                                                                                            |
| `VERBOSE`              | `YES` / **`NO`**          | Use verbose output for the gitlab runner itself                                                                                                                        |
| `SARUS_VERBOSE`        | `YES` / **`NO`**          | Use verbose output for the container runtime                                                                                                                           |
